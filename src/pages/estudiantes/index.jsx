import { Button, Chip, CircularProgress, DialogActions, makeStyles, Snackbar, TextField, Typography, withStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import PeopleIcon from '@material-ui/icons/People';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import * as estudiantesActions from "../../redux/actions/estudiantes";
import { useDispatch, useSelector } from "react-redux";
import { useForm, Controller } from 'react-hook-form'

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import MuiAlert from '@material-ui/lab/Alert';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Pagination from '@material-ui/lab/Pagination';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';

const useStyles = makeStyles((theme) => ({
    button: {
        margin: theme.spacing(1),
    },
    right: {
        float: "right",
    },
    left: {
        float: "left",
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 1, 2),
    },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
    backgroundColor: theme.palette.primary.main,
    color: theme.palette.common.white,
    },
    body: {
    fontSize: 14,
    },
}))(TableCell);

const StyledPagination = withStyles({
    ul: {
        justifyContent: 'center',
        margin: '1%',
        display: 'flex',
    }
})(Pagination);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}


function Estudiantes() {
    const classes = useStyles();

    const state = useSelector((state) => state.estudiantes);
    const dispatch = useDispatch();
    const loading = state.list.loading;

    const [openDialog, setOpenDialog] = React.useState(false);
    const { register, handleSubmit, errors, control, reset } = useForm();
    const [modalForm, setModalForm] = React.useState({});
    
    const defaultValues = {
        nombres: "",
        apellidos: "",
        dni: "",
        edad: "",
    };

    const [openSnackBar, setOpenSnackBar] = React.useState(false)
    const [severitySnackBar, setSeveritySnackBar] = React.useState('success')
    const [messageSnackBar, setMessageSnackBar] = React.useState('')

    const [page, setPage] = useState(1);
    const [totalPages, setTotalPages] = useState(null)
    const [titleDialog, setTitleDialog] = useState('Registrar');
    const [editValue, setEditValue] = useState(false);

    const [openDialogEliminar, setOpenDialogEliminar] = React.useState(false);

    const [idEliminar, setIdEliminar] = React.useState('');

    const handleClickOpenDialogEliminar = () => {
        setOpenDialogEliminar(true);
      };
    
      const handleCloseDialogEliminar = () => {
        setOpenDialogEliminar(false);
      };
    
      const handlePagination = (event, value) => {
        getDataFromAPI(value - 1);
        setPage(value);
      };
    
      const handleClickSnackBar = (severity, message) => {
        setSeveritySnackBar(severity)
        setMessageSnackBar(message)
        setOpenSnackBar(true);
      };
    
      const handleCloseSnackBar = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }
    
        setOpenSnackBar(false);
      };
    
      const registrarSubmit = async (data) => {
        if (editValue === true) {
          const formUpdate = {
            id: modalForm.id,
            ...data,
            estado: modalForm.estado
          }
          
          const respuesta = await dispatch(estudiantesActions.editEstudiante(formUpdate, state));
          handleCloseDialog()
          if (respuesta) {
            handleClickSnackBar('success', 'Se actualizó correctamente');
          } else {
            handleClickSnackBar('error', 'Ocurrio un error al actualizar');
          }
    
          
        } else {
          const respuesta = await dispatch(estudiantesActions.addNewEstudiante(data, state));
          handleCloseDialog();
          if (respuesta) {
              handleClickSnackBar('success', 'Se registró correctamente');
          } else {
              handleClickSnackBar('error', 'Ocurrio un error');
          }    
        }
    
      }
    
      const handleClickOpenDialog = (title) => {
        setTitleDialog(title)
        setOpenDialog(true);
      };
    
      const handleCloseDialog = () => {
        reset(defaultValues);
        setEditValue(false)
        setModalForm(defaultValues)
        setOpenDialog(false);
      };
    
      const getDataFromAPI = (page) => {
        dispatch(estudiantesActions.getListEstudiantesPagination(page));
      };
    
      const openDialogEditar = (title, data) => {
        setEditValue(true)
        setModalForm(data);
        handleClickOpenDialog(title)
      }
    
      useEffect(() => {
        getDataFromAPI(0);
        // console.log(state.list.data)
      }, []);
    
      useEffect(() => {
        if (state.list.data) {
            // console.log(state.list.data)
            setTotalPages(state.list.data.totalPages)
        }
      }, [state.list.data]);
    
      useEffect(() => {
        reset(modalForm)
      }, [modalForm]);
    
      const onEliminar = (data) => {
        setIdEliminar(data.id);
        handleClickOpenDialogEliminar();
      };
    
      const eliminarRegistro = () => {
        if (idEliminar != '') {
          
          handleCloseDialogEliminar()
          if (dispatch(estudiantesActions.deleteEstudiante(idEliminar, state))) {
            handleClickSnackBar('success', 'Se eliminó correctamente')
          } else {
            handleClickSnackBar('error', 'Ocurrio un error al eliminar')
          }
        }
      }

    return (
        <div>
            <div className={classes.left}>
            <Typography variant="h5" gutterBottom style={{marginTop: '16px'}}>
                <PeopleIcon />  ESTUDIANTES
            </Typography>
            </div>

            <div className={classes.right} >
                <Button
                    variant="outlined"
                    color="primary"
                    size="large"
                    onClick={() => handleClickOpenDialog('Registrar')}
                    className={classes.button}
                    startIcon={<AddCircleIcon />}
                >
                    Agregar Estudiante
                </Button>

            </div>

            <TableContainer component={Paper} >
                <Table className={classes.table} aria-label="customized table">
                    <TableHead>
                        <TableRow >
                            <StyledTableCell align="center">Nombres</StyledTableCell>
                            <StyledTableCell align="center">Apellidos</StyledTableCell>
                            <StyledTableCell align="center">DNI</StyledTableCell>
                            <StyledTableCell align="center">Edad</StyledTableCell>
                            <StyledTableCell align="center">Opciones</StyledTableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {!state.list.data && <CircularProgress color="inherit" />}
                        {state.list.data && state.list.data.content && state.list.data.content.map((row) => (
                        <TableRow  hover key={row.id}>
                            <StyledTableCell align="center">{row.nombres}</StyledTableCell>
                            <StyledTableCell align="center">{row.apellidos}</StyledTableCell>
                            <StyledTableCell align="center">{row.dni}</StyledTableCell>
                            <StyledTableCell align="center">{row.edad}</StyledTableCell>
                            <StyledTableCell align="center">
                                <Button size="small" variant="outlined" color="secondary" style={{margin: '0 5px'}} onClick={() => onEliminar(row)}>
                                    <DeleteIcon fontSize="small" />
                                </Button>
                                <Button size="small" variant="outlined" color="warn" style={{margin: '0 5px'}} onClick={() => openDialogEditar('Editar', row)}>
                                    <EditIcon />
                                </Button>
                            </StyledTableCell>
                        </TableRow >
                        ))}
                    </TableBody>
                </Table>
                
                <div >
                    {state.list.data && state.list.data.totalPages > 1 && <StyledPagination count={totalPages} 
                                color="primary" 
                                size="large" 
                                onChange={handlePagination} />}
                </div>
            </TableContainer>

            {/* Modal */}
            <div>
                <Dialog
                open={openDialog}
                TransitionComponent={Transition}
                keepMounted
                onClose={handleCloseDialog}
                aria-labelledby="alert-dialog-slide-title"
                aria-describedby="alert-dialog-slide-description"
                >
                <DialogTitle id="alert-dialog-slide-title">
                    {titleDialog + ' Estudiante'}
                </DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-slide-description">
                    <form className={classes.form} onSubmit={handleSubmit(registrarSubmit)}>
                        <Controller
                            name='nombres'
                            id='nombres'
                            control={control}
                            defaultValue={modalForm.nombres != null ? modalForm.nombres: ''}
                            rules={{
                                required: 'Campo Requerido',
                            }}
                            render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                //disabled={loading}
                                error={errors.nombres && errors.nombres.type ? true: false}
                                helperText={errors.nombres && errors.nombres.type ? errors.nombres.message: null}
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='Nombres'
                                autoComplete='nombres'
                                autoFocus
                            />
                            )}
                        />
                        <Controller
                            name='apellidos'
                            id='apellidos'
                            control={control}
                            defaultValue={modalForm.apellidos != null ? modalForm.apellidos: ''}
                            rules={{
                                required: 'Campo Requerido',
                            }}
                            render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                // disabled={loading}
                                error={errors.apellidos && errors.apellidos.type ? true: false}
                                helperText={errors.apellidos && errors.apellidos.type ? errors.apellidos.message: null}
                                type='text'
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='Apellidos'
                                autoComplete='apellidos'
                            />
                            )}
                        />
                        <Controller
                            name='dni'
                            id='dni'
                            control={control}
                            defaultValue={modalForm.dni != null ? modalForm.dni: ''}
                            rules={{
                                required: 'Campo Requerido',
                            }}
                            render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                // disabled={loading}
                                error={errors.dni && errors.dni.type ? true: false}
                                helperText={errors.dni && errors.dni.type ? errors.dni.message: null}
                                type='text'
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='DNI'
                                autoComplete='dni'
                            />
                            )}
                        />

                        <Controller
                            name='edad'
                            id='edad'
                            control={control}
                            defaultValue={modalForm.edad != null ? modalForm.edad: ''}
                            rules={{
                                required: 'Campo Requerido',
                            }}
                            render={({ onChange, value }) => (
                            <TextField
                                variant='outlined'
                                margin='normal'
                                // disabled={loading}
                                error={errors.edad && errors.edad.type ? true: false}
                                helperText={errors.edad && errors.edad.type ? errors.edad.message: null}
                                type='text'
                                fullWidth
                                onChange={onChange}
                                value={value}
                                label='Edad'
                                autoComplete='edad'
                            />
                            )}
                        />

                        <Button
                            type='submit'
                            variant='contained'
                            color='primary'
                            // disabled={loading}
                            className={classes.button}
                        >
                            {titleDialog}
                        </Button>

                        <Button onClick={handleCloseDialog} 
                                color="secondary" 
                                variant="contained"
                                className={classes.button}>
                            Cancelar
                        </Button>
                        
                    </form>
                    </DialogContentText>
                </DialogContent>
                </Dialog>
            </div>
            <Snackbar 
                open={openSnackBar} 
                autoHideDuration={2000} 
                onClose={handleCloseSnackBar} >
                    <Alert onClose={handleCloseSnackBar} severity={severitySnackBar}>
                        {messageSnackBar}
                    </Alert>
                </Snackbar>
            
            <Dialog
            open={openDialogEliminar}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleCloseDialogEliminar}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
            >
            <DialogTitle id="alert-dialog-slide-title">
                {"Eliminar Registro"}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                ¿Está seguro que desea eliminar el registro?
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                <Button onClick={handleCloseDialogEliminar} color="default" ariant="contained">
                Cancelar
                </Button>
                <Button onClick={eliminarRegistro} color="secondary" variant="contained">
                Eliminar
                </Button>
            </DialogActions>
            </Dialog>
        </div>
    );
}

export default Estudiantes
