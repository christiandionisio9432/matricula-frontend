import { Button, Chip, CircularProgress, DialogActions, makeStyles, Snackbar, TextField, Typography, withStyles } from "@material-ui/core";
import React, { useEffect, useState } from "react";
import BookIcon from "@material-ui/icons/Book";
import AddCircleIcon from "@material-ui/icons/AddCircle";
import * as cursosActions from "../../redux/actions/cursos";
import { useDispatch, useSelector } from "react-redux";
import { useForm, Controller } from 'react-hook-form'

import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Slide from '@material-ui/core/Slide';
import MuiAlert from '@material-ui/lab/Alert';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Pagination from '@material-ui/lab/Pagination';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';


const useStyles = makeStyles((theme) => ({
  button: {
    margin: theme.spacing(1),
  },
  right: {
    float: "right",
  },
  left: {
    float: "left",
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 1, 2),
  },
}));

const StyledTableCell = withStyles((theme) => ({
    head: {
      backgroundColor: theme.palette.primary.main,
      color: theme.palette.common.white,
    },
    body: {
      fontSize: 14,
    },
  }))(TableCell);
  
  const StyledPagination = withStyles({
    ul: {
        justifyContent: 'center',
        margin: '1%',
        display: 'flex',
    }
  })(Pagination);

const Transition = React.forwardRef(function Transition(props, ref) {
    return <Slide direction="up" ref={ref} {...props} />;
});

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

function Cursos() {
  const classes = useStyles();

  const state = useSelector((state) => state.cursos);
  const dispatch = useDispatch();
  const loading = state.list.loading;

  const [openDialog, setOpenDialog] = React.useState(false);
  const { register, handleSubmit, errors, control, reset } = useForm();
  const [modalForm, setModalForm] = React.useState({});
  
  const defaultValues = {
    nombre: "",
    siglas: ""
  };

  const [openSnackBar, setOpenSnackBar] = React.useState(false)
  const [severitySnackBar, setSeveritySnackBar] = React.useState('success')
  const [messageSnackBar, setMessageSnackBar] = React.useState('')

  const [page, setPage] = useState(1);
  const [totalPages, setTotalPages] = useState(null)
  const [titleDialog, setTitleDialog] = useState('Registrar');
  const [editValue, setEditValue] = useState(false);

  const [openDialogEliminar, setOpenDialogEliminar] = React.useState(false);

  const [idEliminar, setIdEliminar] = React.useState('');

  const handleClickOpenDialogEliminar = () => {
    setOpenDialogEliminar(true);
  };

  const handleCloseDialogEliminar = () => {
    setOpenDialogEliminar(false);
  };

  const handlePagination = (event, value) => {
    getDataFromAPI(value - 1);
    setPage(value);
  };

  const handleClickSnackBar = (severity, message) => {
    setSeveritySnackBar(severity)
    setMessageSnackBar(message)
    setOpenSnackBar(true);
  };

  const handleCloseSnackBar = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenSnackBar(false);
  };

  const registrarSubmit = (data) => {
    if (editValue === true) {
      const formUpdate = {
        id: modalForm.id,
        ...data,
        estado: modalForm.estado
      }
      
      const respuesta = dispatch(cursosActions.editCurso(formUpdate, state));
      handleCloseDialog()
      if (respuesta) {
        handleClickSnackBar('success', 'Se actualizó correctamente');
      } else {
        handleClickSnackBar('error', 'Ocurrio un error al actualizar');
      }

      
    } else {
      const respuesta = dispatch(cursosActions.addNewCurso(data, state));
      handleCloseDialog();
      if (respuesta) {
          handleClickSnackBar('success', 'Se registró correctamente');
      } else {
          handleClickSnackBar('error', 'Ocurrio un error');
      }    
    }

  }

  const handleClickOpenDialog = (title) => {
    setTitleDialog(title)
    setOpenDialog(true);
  };

  const handleCloseDialog = () => {
    reset(defaultValues);
    setEditValue(false)
    setModalForm(defaultValues)
    setOpenDialog(false);
  };

  const getDataFromAPI = (page) => {
    dispatch(cursosActions.getListCursosPagination(page));
  };

  const openDialogEditar = (title, data) => {
    // console.log(data);
    setEditValue(true)
    // console.log(newForm);
    setModalForm(data);
    handleClickOpenDialog(title)
  }

  useEffect(() => {
    getDataFromAPI(0);
  }, []);

  useEffect(() => {
    if (state.list.data) {
        // console.log(state.list.data);
        setTotalPages(state.list.data.totalPages)
    }
  }, [state.list.data]);

  useEffect(() => {
    // console.log(modalForm)
    reset(modalForm)
  }, [modalForm]);

  const onEliminar = (data) => {
    setIdEliminar(data.id);
    handleClickOpenDialogEliminar();
  };

  const eliminarRegistro = () => {
    if (idEliminar != '') {
      
      handleCloseDialogEliminar()
      if (dispatch(cursosActions.deleteCurso(idEliminar, state))) {
        handleClickSnackBar('success', 'Se eliminó correctamente')
      } else {
        handleClickSnackBar('error', 'Ocurrio un error al eliminar')
      }
    }
  }

  return (
    <div>
        <div className={classes.left}>
            <Typography variant="h5" gutterBottom style={{ marginTop: "16px" }}>
            <BookIcon /> CURSOS
            </Typography>
        </div>

        <div className={classes.right}>
            <Button
            onClick={() => handleClickOpenDialog('Registrar')}
            variant="outlined"
            color="primary"
            size="large"
            className={classes.button}
            startIcon={<AddCircleIcon />}
            >
            Agregar Curso
            </Button>
        </div>

        <TableContainer component={Paper} >
            <Table className={classes.table} aria-label="customized table">
            <TableHead>
                <TableRow >
                    <StyledTableCell align="center">Nombre</StyledTableCell>
                    <StyledTableCell align="center">Siglas</StyledTableCell>
                    <StyledTableCell align="center">Estado</StyledTableCell>
                    <StyledTableCell align="center">Opciones</StyledTableCell>
                </TableRow>
            </TableHead>
            <TableBody>
                {!state.list.data && <CircularProgress color="inherit" />}
                {state.list.data && state.list.data.content && state.list.data.content.map((row) => (
                <TableRow  hover key={row.id}>
                    <StyledTableCell align="center">{row.nombre}</StyledTableCell>
                    <StyledTableCell align="center">{row.siglas}</StyledTableCell>
                    <StyledTableCell align="center">
                    {row.siglas && <Chip label="Habilitado" color="default" />}
                    {!row.siglas && <Chip label="Inhabilitado" color="secondary" />}
                    </StyledTableCell>
                    <StyledTableCell align="center">
                        <Button size="small" variant="outlined" color="secondary" style={{margin: '0 5px'}} onClick={() => onEliminar(row)}>
                            <DeleteIcon fontSize="small" />
                        </Button>
                        <Button size="small" variant="outlined" color="warn" style={{margin: '0 5px'}} onClick={() => openDialogEditar('Editar', row)}>
                            <EditIcon />
                        </Button>
                    </StyledTableCell>
                </TableRow >
                ))}
            </TableBody>
            </Table>
            
            <div >
                {state.list.data && state.list.data.totalPages > 1 && <StyledPagination count={totalPages} 
                            color="primary" 
                            size="large" 
                            onChange={handlePagination} />}
            </div>
        </TableContainer>

        {/* Modal */}
        <div>
            <Dialog
            open={openDialog}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleCloseDialog}
            aria-labelledby="alert-dialog-slide-title"
            aria-describedby="alert-dialog-slide-description"
            >
            <DialogTitle id="alert-dialog-slide-title">
                {titleDialog + ' Curso'}
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="alert-dialog-slide-description">
                <form className={classes.form} onSubmit={handleSubmit(registrarSubmit)}>
                    <Controller
                        name='nombre'
                        id='nombre'
                        control={control}
                        defaultValue={modalForm.nombre != null ? modalForm.nombre: ''}
                        rules={{
                            required: 'Campo Requerido',
                          }}
                        render={({ onChange, value }) => (
                        <TextField
                            variant='outlined'
                            margin='normal'
                            //disabled={loading}
                            error={errors.nombre && errors.nombre.type ? true: false}
                            helperText={errors.nombre && errors.nombre.type ? errors.nombre.message: null}
                            fullWidth
                            onChange={onChange}
                            value={value}
                            label='Nombre'
                            autoComplete='nombre'
                            autoFocus
                        />
                        )}
                    />
                    <Controller
                        name='siglas'
                        id='siglas'
                        control={control}
                        defaultValue={modalForm.siglas != null ? modalForm.siglas: ''}
                        rules={{
                            required: 'Campo Requerido',
                          }}
                        render={({ onChange, value }) => (
                        <TextField
                            variant='outlined'
                            margin='normal'
                            // disabled={loading}
                            error={errors.nombre && errors.nombre.type ? true: false}
                            helperText={errors.nombre && errors.nombre.type ? errors.nombre.message: null}
                            type='text'
                            fullWidth
                            onChange={onChange}
                            value={value}
                            label='Siglas'
                            autoComplete='siglas'
                        />
                        )}
                    />

                    <Button
                        type='submit'
                        variant='contained'
                        color='primary'
                        // disabled={loading}
                        className={classes.button}
                    >
                        {titleDialog}
                    </Button>

                    <Button onClick={handleCloseDialog} 
                            color="secondary" 
                            variant="contained"
                            className={classes.button}>
                        Cancelar
                    </Button>
                    
                </form>
                </DialogContentText>
            </DialogContent>
            </Dialog>
        </div>
        <Snackbar 
            open={openSnackBar} 
            autoHideDuration={2000} 
            onClose={handleCloseSnackBar} >
                <Alert onClose={handleCloseSnackBar} severity={severitySnackBar}>
                    {messageSnackBar}
                </Alert>
            </Snackbar>
        
        <Dialog
          open={openDialogEliminar}
          TransitionComponent={Transition}
          keepMounted
          onClose={handleCloseDialogEliminar}
          aria-labelledby="alert-dialog-slide-title"
          aria-describedby="alert-dialog-slide-description"
          >
          <DialogTitle id="alert-dialog-slide-title">
            {"Eliminar Registro"}
          </DialogTitle>
          <DialogContent>
            <DialogContentText id="alert-dialog-slide-description">
              ¿Está seguro que desea eliminar el registro?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={handleCloseDialogEliminar} color="default" ariant="contained">
              Cancelar
            </Button>
            <Button onClick={eliminarRegistro} color="secondary" variant="contained">
              Eliminar
            </Button>
          </DialogActions>
        </Dialog>
    </div>
  );
}

export default Cursos;
