import React from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { useForm, Controller } from 'react-hook-form'
import { useHistory } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux'
import * as authReducer from '../../redux/actions/auth'
import { Alert } from '@material-ui/lab';
import { CircularProgress } from '@material-ui/core';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%',
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


function Login() {
    const classes = useStyles();
    const { register, handleSubmit, errors, control } = useForm();
    const history = useHistory();

    const auth = useSelector((state) => state.auth);
    const dispatch = useDispatch();

    const loginSubmit = (data) => {
      dispatch(
        authReducer.doLogin(
          data,
          () => history.push('/cursos')
        )
      )
    }

    const loading = auth.authenticated.loading

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Login Matrícula
        </Typography>
        {auth.authenticated.error.error && (
          <Alert severity="warning" style={{ marginTop: '10px', marginBottom: '10px' }}>
            {auth.authenticated.error.message}
          </Alert>
        )}
        <form className={classes.form} noValidate onSubmit={handleSubmit(loginSubmit)}>
        <Controller
            name='username'
            id='username'
            control={control}
            defaultValue=''
            render={({ onChange, value }) => (
              <TextField
                variant='outlined'
                margin='normal'
                required
                disabled={loading}
                fullWidth
                onChange={onChange}
                value={value}
                label='Nombre de usuario'
                autoComplete='username'
                autoFocus
              />
            )}
          />
          {errors.email && <span>Email es requerido</span>}
          <Controller
            name='password'
            id='password'
            control={control}
            defaultValue=''
            render={({ onChange, value }) => (
              <TextField
                variant='outlined'
                margin='normal'
                disabled={loading}
                required
                type='password'
                fullWidth
                onChange={onChange}
                value={value}
                label='Password'
                autoComplete='current-password'
              />
            )}
          />
          {errors.password && <span>Password es requerido</span>}
          <Button
            type='submit'
            fullWidth
            variant='contained'
            color='primary'
            disabled={loading}
            className={classes.submit}
          >
            {loading ? <CircularProgress size={14} /> : 'Iniciar Sesión'}
          </Button>
          
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
                Forgot password?
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2">
                {"Don't have an account? Sign Up"}
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
    </Container>
  );
}

export default Login
