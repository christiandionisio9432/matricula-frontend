import { React } from "react";
import Rutas from "./router";

function App() {
  return(
    <Rutas />
  );
}

export default App;
