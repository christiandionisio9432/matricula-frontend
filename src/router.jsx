import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

import Cursos from "./pages/cursos";
import Estudiantes from "./pages/estudiantes";
import Matricula from "./pages/matricula";
import Dashboard from './components/layout';
import Login from "./pages/login";
import { useSelector } from "react-redux";

function Rutas() {
  const auth = useSelector((state) => state.auth)
  const userAuthenticated = auth.authenticated.data

  const PrivateRoute = (props) => {
    if (userAuthenticated) {
      return <Route {...props} />
    }
    return <Redirect to='/login' />
  }

  return (
    <Router>
        <Dashboard>
            <Switch>  
              <Route exact path='/' component={() => <Redirect to={userAuthenticated ? '/cursos' : '/login'} />} />
              <Route exact path='/login' component={Login} />
              <PrivateRoute exact path="/cursos" component={Cursos} />
              <PrivateRoute exact path="/estudiantes" component={Estudiantes} />
              <PrivateRoute exact path="/matricula" component={Matricula} />
              <PrivateRoute exact component={() => <h1>Not Found 404</h1>} />
            </Switch>
        </Dashboard>
    </Router>
  );
}

export default Rutas;