import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Grid from '@material-ui/core/Grid';
import { Chip } from '@material-ui/core';

const products = [
  { name: 'Product 1', desc: 'A nice thing', price: '$9.99' },
  { name: 'Product 2', desc: 'Another thing', price: '$3.45' },
  { name: 'Product 3', desc: 'Something else', price: '$6.51' },
  { name: 'Product 4', desc: 'Best thing of all', price: '$14.11' },
  { name: 'Shipping', desc: '', price: 'Free' },
];
const addresses = ['1 Material-UI Drive', 'Reactville', 'Anytown', '99999', 'USA'];
const payments = [
  { name: 'Card type', detail: 'Visa' },
  { name: 'Card holder', detail: 'Mr John Smith' },
  { name: 'Card number', detail: 'xxxx-xxxx-xxxx-1234' },
  { name: 'Expiry date', detail: '04/2024' },
];

const useStyles = makeStyles((theme) => ({
  listItem: {
    padding: theme.spacing(1, 0),
  },
  total: {
    fontWeight: 700,
  },
  title: {
    marginTop: theme.spacing(2),
  },
}));

export default function Review(props) {
  const classes = useStyles();

  useEffect(() => {
    console.log("data en el review")
    console.log(props.dataFormulario)
  }, [])

  return (
    <React.Fragment>
      <Typography variant="h6" gutterBottom>
        Cursos a Matricular
      </Typography>
      <List disablePadding>
        {props.dataFormulario && props.dataFormulario.listaCursos.map((curso) => (
          <ListItem className={classes.listItem} key={curso.id}>
            <ListItemText primary={curso.nombre} secondary={curso.siglas} />
            <Typography variant="body2">
            {curso.estado && <Chip label="Habilitado" color="default" />}
            {!curso.estado && <Chip label="Inhabilitado" color="secondary" />}
            </Typography>
          </ListItem>
        ))}
        <ListItem className={classes.listItem}>
          <ListItemText primary="Total Cursos" />
          <Typography variant="subtitle1" className={classes.total}>
            { props.dataFormulario.listaCursos.length}
          </Typography>
        </ListItem>
      </List>
        <hr/>
      <Grid container spacing={2}>
        <Grid item xs={12} sm={12}>
          <Typography variant="h6" gutterBottom className={classes.title}>
            Datos del Estudiante
          </Typography>
          <Typography gutterBottom>Nombres:  {props.dataFormulario.estudiante.nombres + ' ' + props.dataFormulario.estudiante.apellidos }</Typography>
          <Typography gutterBottom> DNI: {props.dataFormulario.estudiante.dni}</Typography>
          <Typography gutterBottom> Edad: {props.dataFormulario.estudiante.edad}</Typography>
        </Grid>
      </Grid>
    </React.Fragment>
  );
}