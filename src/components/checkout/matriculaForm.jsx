import React, { useEffect, useState } from 'react';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import { Avatar, Button, IconButton, List, ListItem, ListItemAvatar, ListItemSecondaryAction, ListItemText, makeStyles, MenuItem, Select, useTheme } from '@material-ui/core';
import { Autocomplete } from '@material-ui/lab';
import * as estudiantesActions from "../../redux/actions/estudiantes";
import * as cursosActions from "../../redux/actions/cursos";
import { useSelector } from 'react-redux';
import { useDispatch } from 'react-redux';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        maxWidth: 752,
    },
    demo: {
        backgroundColor: theme.palette.background.paper,
    },
    title: {
        margin: theme.spacing(4, 0, 2),
    },
}));

function generate(element) {
    return [0, 1, 2].map((value) =>
        React.cloneElement(element, {
        key: value,
        }),
    );
}

export default function MatriculaForm(props) {
    const classes = useStyles();
    const [dense, setDense] = React.useState(false);

    const stateEstudiante = useSelector((stateEstudiante) => stateEstudiante.estudiantes);
    const stateCurso = useSelector((stateEstudiante) => stateEstudiante.cursos);
    const dispatch = useDispatch();

    const [cursoSelecionado, setCursoSeleccionado] = useState(null)
    const [estudianteSelecionado, setEstudianteSeleccionado] = useState(null)
    const [listCursoSelect, setListCursoSelect] = useState([]);

    const getDataEstudianteFromAPI = (page) => {
        dispatch(estudiantesActions.getListEstudiantes());
    };

    const getDataCursosFromAPI = (page) => {
        dispatch(cursosActions.getListCursos());
    };

    useEffect(() => {
        getDataEstudianteFromAPI()
        getDataCursosFromAPI()
        sendData();
    }, [])

    useEffect(() => {
        sendData()
    }, [listCursoSelect, estudianteSelecionado])

    const renderList = () => {

        if (listCursoSelect.length === 0) {
            return <div>La matricula no tiene cursos seleccionados</div>
        }
        return listCursoSelect.map( (curso) => (
            <ListItem key={curso.id}> 
                <ListItemAvatar>
                    <Avatar>
                        <FolderIcon />
                    </Avatar>
                </ListItemAvatar>
                <ListItemText
                    primary={curso.nombre}
                />
                <ListItemSecondaryAction>
                    <IconButton edge="end" aria-label="delete" onClick={() => onDelete(curso)}>
                        <DeleteIcon />
                    </IconButton>
                </ListItemSecondaryAction>
            </ListItem>
        ))
    }

    const agregarCurso = () => {
        if (cursoSelecionado === null) {
            return;
        }
        setListCursoSelect(oldArray => [...oldArray, cursoSelecionado])
        setCursoSeleccionado(null)
    }

    const onDelete = (data) => {
        setListCursoSelect(listCursoSelect.filter(curso =>  curso.id !== data.id))
    }

    const sendData = () => {
        props.parentCallback({
            estudiante: estudianteSelecionado,
            listaCursos: listCursoSelect,
            estado: true,
        });
    }

    
    return (
        <React.Fragment>
        <Typography variant="h6" gutterBottom>
            Datos de Matrícula
        </Typography>
        <Grid container spacing={3}>
            <Grid item xs={12}>
            <Autocomplete
                id="combo-box-estudiante"
                options={stateEstudiante.list.data || []}
                getOptionLabel={(estudiante) => estudiante.dni + ' - ' + estudiante.nombres + ' ' + estudiante.apellidos}
                onChange={(event, value) => setEstudianteSeleccionado(value)} 
                fullWidth
                renderInput={(params) => <TextField {...params} label="Seleccionar Estudiante" variant="outlined" />}
                
            />
            </Grid>
            <Grid item xs={12} sm={8}>
                <Autocomplete
                    id="combo-box-curso"
                    options={stateCurso.list.data || []}
                    getOptionLabel={(curso) => curso.siglas + ' - ' + curso.nombre}
                    onChange={(event, value) => setCursoSeleccionado(value)} 
                    fullWidth
                    renderInput={(params) => <TextField {...params} label="Seleccionar Curso" variant="outlined" />}
                    
                />
            </Grid>
            <Grid item xs={12} sm={4}>
                <Button variant="contained" color="primary" onClick={agregarCurso} >
                    Agregar Curso
                </Button>
            </Grid>
            <Grid item xs={12}>
                <Typography variant="h6" className={classes.title}>
                    Lista de Cursos
                </Typography>
                <div className={classes.demo}>
                    <List dense={dense}>
                        {(listCursoSelect !== []) && renderList()}
                    </List>
                </div>
            </Grid>
        </Grid>
        </React.Fragment>
    );
}

