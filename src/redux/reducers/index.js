import { combineReducers } from 'redux';
import cursos from './cursos'
import estudiantes from './estudiantes'
import auth from './auth'
import matricula from './matricula'

export default function createReducer(injectedReducers = {}) {
  const rootReducer = combineReducers({
    ...injectedReducers,
    cursos,
    estudiantes,
    auth,
    matricula
  });

  return rootReducer;
}
