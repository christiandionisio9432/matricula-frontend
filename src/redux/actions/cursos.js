import * as TYPE from '../types/cursos'
import baseApi from '../../api/baseApi'
import { ENDPOINT_CURSOS } from '../../utils/endpoints'
import { cursoPorPagina } from '../../utils/constants'

export const getListCursos = () => async (dispatch) => {
  try {
    dispatch({ type: TYPE.GET_CURSOS_START })

    const response = await baseApi.get(ENDPOINT_CURSOS)

    dispatch({
      type: TYPE.GET_CURSOS_SUCCESS,
      data: response.data
    })
  } catch (_) {
    dispatch({
      type: TYPE.GET_CURSOS_FAIL,
      error: {
        error: true,
        message: 'Ocurrió un error al obtener cursos'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_CURSOS_FINISH })
  }
}

export const getListCursosPagination = (page = 0) => async (dispatch) => {
  try {
    dispatch({ type: TYPE.GET_CURSOS_START })

    const response = await baseApi.get(`${ENDPOINT_CURSOS}/pageable?page=${page}&size=${cursoPorPagina}`)

    dispatch({
      type: TYPE.GET_CURSOS_SUCCESS,
      data: response.data
    })
  } catch (_) {
    dispatch({
      type: TYPE.GET_CURSOS_FAIL,
      error: {
        error: true,
        message: 'Ocurrió un error al obtener cursos'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_CURSOS_FINISH })
  }
}

export const deleteCurso = (idCurso, stateCursos) => async (dispatch) => {
  // const arrayCursosList = stateCursos.list.data
  let respuesta = false;
  dispatch({ type: TYPE.DELETE_CURSOS_START })
  try {
    const endPointDeleteCurso = `${ENDPOINT_CURSOS}/${idCurso}`
    await baseApi.delete(endPointDeleteCurso)

    // const newData = arrayCursosList.content.filter((obj) => obj.id !== idCurso)
    const newData = await baseApi.get(`${ENDPOINT_CURSOS}/pageable?page=${stateCursos.list.data.pageNumber}&size=${cursoPorPagina}`)

    dispatch({
      type: TYPE.DELETE_CURSOS_SUCCESS,
      data: newData.data
    })
    respuesta = true;
  } catch (error) {
    dispatch({
      type: TYPE.DELETE_CURSOS_FAIL,
      error: {
        error: true,
        message: 'Error al eliminar curso'
      }
    })
  } finally {
    dispatch({ type: TYPE.DELETE_CURSOS_FINISH })
    return respuesta;
  }
}

export const addNewCurso = (dataForm, stateCursos) => async (dispatch) => {
  const arrayCursosList = stateCursos.list.data
  let respuesta = false
  dispatch({ type: TYPE.ADD_CURSOS_START })
  try {
    dataForm.estado = true
    const response = await baseApi.post(ENDPOINT_CURSOS, dataForm)
    arrayCursosList.content.push(response.data);

    dispatch({
      type: TYPE.ADD_CURSOS_SUCCESS,
      data: arrayCursosList
    })
    respuesta = true
  } catch (error) {
    dispatch({
      type: TYPE.ADD_CURSOS_FAIL,
      error: {
        error: true,
        message: 'Error al agregar curso'
      }
    })
  } finally {
    dispatch({ type: TYPE.ADD_CURSOS_FINISH })
    return respuesta
  }
}

export const editCurso = (dataForm, stateCursos) => async (dispatch) => {
  const arrayCursosList = stateCursos.list.data
  let respuesta = false
  dispatch({ type: TYPE.EDIT_CURSOS_START })
  try {
    const endPointUpdate = `${ENDPOINT_CURSOS}/${dataForm.id}`
    await baseApi.put(endPointUpdate, dataForm)

    const index = arrayCursosList.content.findIndex((curso) => curso.id === dataForm.id)
    const newData = arrayCursosList
    newData.content[index] = dataForm

    dispatch({
      type: TYPE.EDIT_CURSOS_SUCCESS,
      data: newData
    })
    respuesta = true
  } catch (error) {
    dispatch({
      type: TYPE.EDIT_CURSOS_FAIL,
      error: {
        error: true,
        message: 'Error al editar curso'
      }
    })
  } finally {
    dispatch({ type: TYPE.EDIT_CURSOS_FINISH })
    return respuesta
  }
}
