import { ENDPOINT_BOLETAS, ENDPOINT_BOLETAS_USUARIO, ENDPOINT_CLIENTES, ENDPOINT_PLATOS } from '../../utils/endpoints'
import baseApi from '../../api/baseApi'
import * as TYPE from '../types/matricula'

export const getListBoletas = () => async (dispatch) => {
  try {
    dispatch({ type: TYPE.GET_BOLETAS_START })

    const response = await baseApi.get(ENDPOINT_BOLETAS)

    dispatch({
      type: TYPE.GET_BOLETAS_SUCCESS,
      data: response.data
    })
  } catch (_) {
    dispatch({
      type: TYPE.GET_BOLETAS_FAIL,
      error: {
        error: true,
        message: 'Ocurrió un error al obtener las boletas'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_BOLETAS_FINISH })
  }
}

export const getListBoletasByUser = (id) => async (dispatch) => {
  try {
    dispatch({ type: TYPE.GET_BOLETAS_USER_START })

    const payload = {
      idCliente: id
    }

    const response = await baseApi.post(ENDPOINT_BOLETAS_USUARIO, payload)

    dispatch({
      type: TYPE.GET_BOLETAS_USER_SUCCESS,
      data: response.data
    })
  } catch (_) {
    dispatch({
      type: TYPE.GET_BOLETAS_USER_FAIL,
      error: {
        error: true,
        message: 'Ocurrió un error al obtener las boletas de usuario'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_BOLETAS_USER_FINISH })
  }
}

export const clearListBoletasByUser = () => (dispatch) => {
  dispatch({ type: TYPE.CLEAR_BOLETAS_USER })
}

export const getDetailBoleta = (idBoleta, callback) => async (dispatch) => {
  dispatch({ type: TYPE.GET_BOLETA_DETAIL_START })
  try {
    const response = await baseApi.get(`${ENDPOINT_BOLETAS}/${idBoleta}`)

    const idCliente = response.data.cliente.id
    const responseClient = await baseApi.get(`${ENDPOINT_CLIENTES}/${idCliente}`)
    
    const listPromisesPlatos = []

    response.data.items.map((p) => {
      const req = baseApi.get(`${ENDPOINT_PLATOS}/${p.plato.id}`)
      listPromisesPlatos.push(req)
    })

    const responseAllPlatos = await Promise.all(listPromisesPlatos)

    const filterResponsePlatos = responseAllPlatos.map((p) => {
      return p.data
      })

    const jsonResponse = {
      general: response.data,
      cliente: responseClient.data,
      platos: filterResponsePlatos
    }

    dispatch({
      type: TYPE.GET_BOLETA_DETAIL_SUCCESS,
      data: jsonResponse
    })
    callback()
  } catch (error) {
    dispatch({
      type: TYPE.GET_BOLETA_DETAIL_FAIL,
      error: {
        error: false,
        message: 'Ocurrió un error al obtener detalle de boleta'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_BOLETA_DETAIL_FINISH })
  }
}
