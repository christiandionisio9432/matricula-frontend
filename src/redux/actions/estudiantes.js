import baseApi from '../../api/baseApi'
import { ENDPOINT_ESTUDIANTES } from '../../utils/endpoints'
import store from '../store'
import * as TYPE from '../types/estudantes'
import { estudiantesPorPagina } from '../../utils/constants'

export const getListEstudiantes = () => async (dispatch) => {
  dispatch({ type: TYPE.GET_ESTUDIANTE_START })
  try {
    const response = await baseApi.get(ENDPOINT_ESTUDIANTES)
    dispatch({
      type: TYPE.GET_ESTUDIANTE_SUCCESS,
      data: response.data
    })
  } catch (error) {
    dispatch({
      type: TYPE.GET_ESTUDIANTE_FAIL,
      error: {
        error: true,
        message: 'Error al obtener estudiantes'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_ESTUDIANTE_FINISH })
  }
}

export const getListEstudiantesPagination = (page = 0) => async (dispatch) => {
  try {
    dispatch({ type: TYPE.GET_ESTUDIANTE_START })

    const response = await baseApi.get(`${ENDPOINT_ESTUDIANTES}/pageable?page=${page}&size=${estudiantesPorPagina}`)

    dispatch({
      type: TYPE.GET_ESTUDIANTE_SUCCESS,
      data: response.data
    })
  } catch (_) {
    dispatch({
      type: TYPE.GET_ESTUDIANTE_FAIL,
      error: {
        error: true,
        message: 'Ocurrió un error al obtener cursos'
      }
    })
  } finally {
    dispatch({ type: TYPE.GET_ESTUDIANTE_FINISH })
  }
}

export const deleteEstudiante = (idEstudiante, stateEstudiante) => async (dispatch) => {
  let respuesta = false;
  dispatch({ type: TYPE.DELETE_ESTUDIANTE_START })
  try {
    const endPointDeleteEstudiante = `${ENDPOINT_ESTUDIANTES}/${idEstudiante}`
    await baseApi.delete(endPointDeleteEstudiante)
    
    const newData = await baseApi.get(`${ENDPOINT_ESTUDIANTES}/pageable?page=${stateEstudiante.list.data.pageNumber}&size=${estudiantesPorPagina}`)

    dispatch({
      type: TYPE.DELETE_ESTUDIANTE_SUCCESS,
      data: newData.data
    })
    respuesta = true;
  } catch (error) {
    dispatch({
      type: TYPE.DELETE_ESTUDIANTE_FAIL,
      error: {
        error: true,
        message: 'Error al eliminar estudiante'
      }
    })
  } finally {
    dispatch({ type: TYPE.DELETE_ESTUDIANTE_FINISH })
    return respuesta;
  }
}

export const addNewEstudiante = (dataForm, stateEstudiante) => async (dispatch) => {
  const arrayEstudianteList = stateEstudiante.list.data
  let respuesta = false

  dispatch({ type: TYPE.ADD_ESTUDIANTE_START })
  try {
    const response = await baseApi.post(ENDPOINT_ESTUDIANTES, dataForm)
    arrayEstudianteList.content.push(response.data);

    dispatch({
      type: TYPE.ADD_ESTUDIANTE_SUCCESS,
      data: arrayEstudianteList
    })

    respuesta = true
  } catch (error) {
    dispatch({
      type: TYPE.ADD_ESTUDIANTE_FAIL,
      error: {
        error: true,
        message: 'Error al agregar estudiante'
      }
    })
  } finally {
    dispatch({ type: TYPE.ADD_ESTUDIANTE_FINISH })

    return respuesta
  }
}

export const editEstudiante = (dataForm, stateEstudiante) => async (dispatch) => {
  const arrayEstudianteList = stateEstudiante.list.data
  let respuesta = false
  
  dispatch({ type: TYPE.EDIT_ESTUDIANTE_START })
  try {
    const endPointUpdateEstudiante = `${ENDPOINT_ESTUDIANTES}/${dataForm.id}`
    await baseApi.put(endPointUpdateEstudiante, dataForm)

    const index = arrayEstudianteList.content.findIndex((estudiante) => estudiante.id === dataForm.id)
    const newData = arrayEstudianteList
    newData.content[index] = dataForm
    
    
    dispatch({
      type: TYPE.EDIT_ESTUDIANTE_SUCCESS,
      data: newData
    })
    respuesta = true;
    
  } catch (error) {
    dispatch({
      type: TYPE.EDIT_ESTUDIANTE_FAIL,
      error: {
        error: true,
        message: 'Error al editar estudiante'
      }
    })
    // console.log(error)
  } finally {
    dispatch({ type: TYPE.EDIT_ESTUDIANTE_FINISH })
    return respuesta
  }
}
